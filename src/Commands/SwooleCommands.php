<?php

namespace Drupal\swoole\Commands;

use Consolidation\SiteAlias\SiteAliasManagerAwareInterface;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\swoole\ServerProcessInspector;
use Drupal\swoole\Extension;
use Drush\Commands\core\CacheCommands;
use Drush\Commands\DrushCommands;
use Symfony\Component\Process\PhpExecutableFinder;
use Symfony\Component\Process\Process;


use Drupal\Core\Utility\Error;


/**
 * The added Drush commands.
 */
class SwooleCommands extends DrushCommands implements SiteAliasManagerAwareInterface {

  use SiteAliasManagerAwareTrait;

  /**
   * The config.factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The swoole.server_process_inspector service.
   *
   * @var \Drupal\swoole\ServerProcessInspector
   */
  protected $serverProcessInspector;

  /**
   * The Swoole extension service.
   *
   * @var \Drupal\swoole\Extension
   */
  protected $extension;

  /**
   * SwooleCommands constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config.factory service.
   * @param \Drupal\swoole\ServerProcessInspector $server_process_inspector
   *   The swoole.server_process_inspector service.
   * @param \Drupal\swoole\Extension
   *   The Swoole extension service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ServerProcessInspector $server_process_inspector, Extension $extension) {
    parent::__construct();
    $this->configFactory = $config_factory;
    $this->serverProcessInspector = $server_process_inspector;
    $this->extension = $extension;
  }

  /**
   * @command swoole:status
   */
  public function status() {
    if ($this->serverProcessInspector->serverIsRunning()) {
      $this->logger()->info(dt('The Swoole server is running.'));
    }
    else {
      $this->logger()->info(dt('The Swoole server is not running.'));
    }
  }

  /**
   * @command swoole:start
   * @option cr Execute a cache rebuild before staring the Swoole server.
   * @option host The IP address the server should bind to.
   * @option port The port the server should be available on.
   */
  public function start($options = ['cr' => FALSE, 'host' => NULL, 'port' => NULL]) {
    $swoole_settings = $this->configFactory->getEditable('swoole.settings');
    $site_config = $this->configFactory->get('system.site');

    if (!\Drupal::service('swoole.extension')->isInstalled()) {
      $this->logger()->warning(dt('The Swoole extension is missing.'));
      return;
    }

    if ($this->serverProcessInspector->serverIsRunning()) {
      $this->logger()->warning(dt('The Swoole server is already running.'));
      return;
    }

    if (($swoole_settings->get('ssl') === TRUE) && !defined('SWOOLE_SSL')) {
      $this->logger()->warning(dt('You must configure Swoole with `--enable-openssl` to support ssl.'));
      return;
    }

    // Do a cache rebuild when the option '--cr' is set. Additionally it also
    // clears Drush cache and Drupal's render cache.
    if ($options['cr']) {
      $cache_commands = new CacheCommands();

      // Set the logger instance.
      $reflection_logger = new \ReflectionProperty($cache_commands, 'logger');
      $reflection_logger->setValue($cache_commands, $this->logger());

      $cache_commands->rebuild();
    }

    // Use the value for the option '--host=' when it is used, instead of the
    // one stored in config or the default value.
    $host = $swoole_settings->get('host');
    if (!is_null($options['host'])) {
      $host = $options['host'];
    }

    // Use the value for the option '--port=' when it is used, instead of the
    // one stored in config or the default value.
    $port = $swoole_settings->get('port');
    if (!is_null($options['port'])) {
      $port = $options['port'];
    }

    $server = new Process([
      (new PhpExecutableFinder)->find(), 'swoole-server',
    ], realpath(__DIR__.'/../../bin'), [
      'DRUPAL_ROOT' => DRUPAL_ROOT,
      'SITE_PATH' => $swoole_settings->get('site_path'),
      'SITE_NAME' => $site_config->get('name'),
      'SWOOLE_HOST' => $host,
      'SWOOLE_PORT' => $port,

      'SWOOLE_SSL' => ($swoole_settings->get('ssl') ? 'TRUE' : 'FALSE'),
      'SWOOLE_SSL_CERT_FILE' => $site_config->get('ssl_cert_file'),
      'SWOOLE_SSL_KEY_FILE' => $site_config->get('ssl_KEY_file'),

      'SWOOLE_MAX_EXECUTION_TIME' => $swoole_settings->get('max_execution_time'),
      'SWOOLE_TICKS' => ($swoole_settings->get('ticks') ? 'TRUE' : 'FALSE'),
      'SWOOLE_PACKAGE_MAX_LENGTH' => $swoole_settings->get('package_max_length'),
      'SWOOLE_SOCKER_BUFFER_SIZE' => $swoole_settings->get('socket_buffer_size'),

      // Swoole (task)worker settings.
      'SWOOLE_MAX_REQUEST' => $swoole_settings->get('max_request'),
      'SWOOLE_WORKER_NUM' => $swoole_settings->get('worker_num'),
      'SWOOLE_TASK_MAX_REQUEST' => $swoole_settings->get('task_max_request'),
      'SWOOLE_TASK_WORKER_NUM' => $swoole_settings->get('task_worker_num'),

      // Swoole logging to file.
      'SWOOLE_LOG_LEVEL' => $swoole_settings->get('log_level'),
      'SWOOLE_LOG_FILE' => $swoole_settings->get('log_file'),

      // Daemonize the Swoole Server process.
      'SWOOLE_DAEMONIZE' => ($swoole_settings->get('daemonize') ? 'TRUE' : 'FALSE'),

      // Stream request data to the console.
      'SWOOLE_STREAM_TO_CONSOLE' => ($swoole_settings->get('stream_to_console') ? 'TRUE' : 'FALSE'),
      'SWOOLE_ADD_MEMORY_USAGE' => ($swoole_settings->get('add_memory_usage') ? 'TRUE' : 'FALSE'),
      'SWOOLE_ADD_HANDLE_DURATION' => ($swoole_settings->get('add_handle_duration') ? 'TRUE' : 'FALSE'),
    ]);
    $server->start();

    $swoole_settings
      ->set('master_pid', (int) $server->getPid())
      ->set('manager_pid', -1)
      ->save(TRUE);

    return $this->runServer($server, $this->serverProcessInspector);
  }

  /**
   * Run the given server process.
   *
   * @param \Symfony\Component\Process\Process $server
   * @param \Drupal\swoole\ServerProcessInspector $inspector
   * @return int
   */
  protected function runServer(Process $server, ServerProcessInspector $inspector) {
    while (!$server->isStarted()) {
      sleep(1);
    }

    $settings = $this->configFactory->getEditable('swoole.settings');

    // Write the server start "message" to the console.
    $this->logger()->success(dt('Started the Swoole server on http://@host:@port.', [
      '@host' => $settings->get('host'),
      '@port' => $settings->get('port'),
    ]));

    $this->output()->writeln(dt('Press Ctrl+C to stop the server'));

    try {
      while ($server->isRunning()) {
        $this->writeServerOutput($server);

        usleep(500 * 1000);
      }

      $this->writeServerOutput($server);
    }
    catch (\Exception $e) {
      return;
    }
    finally {
      $inspector->stopServer();
    }

    return $server->getExitCode();
  }

  /**
   * Write the server process output to the console.
   *
   * @param \Symfony\Component\Process\Process $server
   */
  protected function writeServerOutput($server) {
    $output = $server->getIncrementalOutput();
    $output = explode(PHP_EOL, $output);
    foreach ($output as $line) {
      if (!empty($line)) {
        if ($stream = json_decode($line, TRUE)) {
          $stream_output = explode(PHP_EOL, $this->handleStream($stream));
          foreach ($stream_output as $line) {
            if (!empty($line)) {
              $this->output()->writeln($line);
            }
          }
        }
        else {
          $this->output()->writeln($line);
        }
      }
    }

    $error_output = $server->getIncrementalErrorOutput();
    $error_output = explode(PHP_EOL, $error_output);
    foreach ($error_output as $line) {
      if (!empty($line)) {
        if ($stream = json_decode($line, TRUE)) {
          $stream_error_output = explode(PHP_EOL, $this->handleStream($stream));
          foreach ($stream_error_output as $line) {
            if (!empty($line)) {
              $this->logger()->error($line);
            }
          }
        }
        else {
          $this->logger()->error($line);
        }
      }
    }

    $server->clearOutput()->clearErrorOutput();
  }

  /**
   * Handle stream information from the worker.
   *
   * @param array $stream
   * @param int|string|NULL $verbosity
   * @return void
   */
  protected function handleStream($stream, $verbosity = NULL) {
    $type = $stream['stream_type'] ?? NULL;

    $output = '';
    switch ($type) {
      case 'request':
        $output = static::placeholderEscape($stream['method']) . ': ' . static::placeholderEscape($stream['url']);

        $memory_usage = '';
        if (isset($stream['memory']) && ($stream['memory'] > 0)) {
          $memory_usage = 'memory usage: ' . (number_format($stream['memory'] / 1024 / 1024, 2, '.', '').' mb');
        }

        $handle_duration = '';
        if (isset($stream['duration']) && ($stream['duration'] > 0)) {
          $handle_duration = 'duration: ' . number_format(round($stream['duration'], 3), 3, '.', '') . ' seconds';
        }

        if (!empty($memory_usage) && !empty($handle_duration)) {
          $output .= ' (' . $memory_usage . ', ' . $handle_duration . ')';
        }
        elseif (!empty($memory_usage)) {
          $output .= ' (' . $memory_usage . ')';
        }
        elseif (!empty($handle_duration)) {
          $output .= ' (' . $handle_duration . ')';
        }
        break;

      case 'throwable':
        $output = static::placeholderEscape($stream['%type']) . ': ';
        $output .= static::placeholderEscape($stream['@message']) . "\n";
        $output .= 'In ' . static::placeholderEscape($stream['%function']) . ' (line ';
        $output .= static::placeholderEscape($stream['%line']) . ' of ';
        $output .= static::placeholderEscape($stream['%file']) . ').' . "\n";

        if (!empty($stream['backtrace'])) {
          $output .= "Stack trace:\n";
          foreach ($stream['backtrace'] as $i => $backtrace) {
            $output .= '#' . $i . ' ' . static::placeholderEscape($stream['%file']) . ' on line ' . static::placeholderEscape($stream['%line']) . "\n";
          }
        }
        break;

      default:
        $output = json_encode($stream, $verbosity);
        break;

    }

    return $output;
  }

  /**
   * Escapes a placeholder replacement value if needed.
   *
   * @param string|\Drupal\Component\Render\MarkupInterface $value
   *   A placeholder replacement value.
   *
   * @return string
   *   The properly escaped replacement value.
   */
  protected static function placeholderEscape($value) {
    return $value instanceof \Drupal\Component\Render\MarkupInterface ? (string) $value : \Drupal\Component\Utility\Html::escape($value);
  }

  /**
   * @command swoole:stop
   */
  public function stop(): bool {
    if (!$this->serverProcessInspector->serverIsRunning()) {
      $this->logger()->warning(dt('The Swoole server is not running.'));
      return TRUE;
    }

    $this->logger()->info(dt('Stopping the Swoole server...'));

    if (!$this->serverProcessInspector->stopServer()) {
      $this->logger()->warning(dt('Failed to stop the Swoole server.'));
      return FALSE;
    }
    else {
      $this->logger()->success(dt('The Swoole server has been stopped.'));
      return TRUE;
    }
  }

  /**
   * @command swoole:restart
   * @option cr Execute a cache rebuild before reloading the Swoole server.
   */
  public function restart($options = ['cr' => FALSE]): void {
    $options += ['cr' => FALSE, 'host' => NULL, 'port' => NULL];
    if ($this->stop()) {
      $this->start($options);
    }
  }

}
