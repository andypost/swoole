<?php

namespace Drupal\swoole\Exceptions;

/**
 * The exception for tasks
 */
class TaskException extends \Exception {

  /**
   * The original throwable class name.
   *
   * @var string
   */
  protected $class;

  /**
   * Constructs a TaskException object.
   *
   * @param string $class
   *   The class name.
   * @param string $message
   *   The message.
   * @param int $code
   *   The code status.
   * @param string $file
   *   The file name.
   * @param int $line
   *   The line number.
   *
   * @return void
   */
  public function __construct($class, $message, $code, $file, $line) {
    parent::__construct($message, $code);

    $this->class = $class;
    $this->file = $file;
    $this->line = $line;
  }

  /**
   * Returns the original throwable class name.
   *
   * @return string
   */
  public function getClass() {
    return $this->class;
  }

}
