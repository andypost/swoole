<?php

namespace Drupal\swoole\Exceptions;

/**
 * Exception for value too large for column.
 */
class ValueTooLargeForColumnException extends \InvalidArgumentException {}
