<?php

namespace Drupal\swoole\Actions;

use Drupal\swoole\Extension;

/**
 * Stop worker processes that exceed the maximum execution time.
 */
class EnsureRequestsDontExceedMaxExecutionTime {

  /**
   * Constructs a EnsureRequestsDontExceedMaxExecutionTime object.
   *
   * @param \Drupal\swoole\Extension $extension
   *   The Swoole extension.
   * @param array $timerTable
   *   The time table.
   * @param int $maxExecutionTime
   *   The maximum execution time.
   */
  public function __construct(
    protected Extension $extension,
    protected $timerTable,
    protected $maxExecutionTime
  ) {}

  /**
   * Invoke the action.
   *
   * @return void
   */
  public function __invoke() {
    foreach ($this->timerTable as $workerId => $row) {
      if ((time() - $row['time']) > $this->maxExecutionTime) {
        $this->timerTable->del($workerId);

        $this->extension->dispatchProcessSignal($row['worker_pid'], SIGKILL);
      }
    }
  }

}
