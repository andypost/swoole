<?php

namespace Drupal\swoole\Actions;

use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * Convert a Swoole request into a Drupal request.
 */
class ConvertSwooleRequestToDrupalRequest {

  /**
   * Convert the given Swoole request into an Drupal request.
   *
   * @param \Swoole\Http\Request $swoole_request
   *   The Swoole request.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   The Drupal request.
   */
  public function __invoke($swoole_request): Request {
    $server_variables = $this->prepareServerVariables(
      $swoole_request->server ?? [],
      $swoole_request->header ?? [],
      $swoole_request->cookie ?? []
    );

    // Copied from Symfony\Component\HttpFoundation\Request::createFromGlobals().
    $request = new Request(
      $swoole_request->get ?? [],
      $swoole_request->post ?? [],
      [],
      $swoole_request->cookie ?? [],
      $swoole_request->files ?? [],
      $server_variables,
      $swoole_request->rawContent(),
    );

    // Copied from Symfony\Component\HttpFoundation\Request::createFromGlobals().
    if (str_starts_with((string) $request->headers->get('CONTENT_TYPE'), 'application/x-www-form-urlencoded') &&
      in_array(strtoupper($request->server->get('REQUEST_METHOD', 'GET')), ['PUT', 'PATCH', 'DELETE'])) {
      parse_str($request->getContent(), $data);

      $request->request = new InputBag($data);
    }

    return $request;
  }

  /**
   * Parse the "server" variables and headers into a single array of $_SERVER variables.
   *
   * @param array $server
   * @param array $headers
   * @param array $cookie
   *
   * @return array
   */
  protected function prepareServerVariables(array $server, array $headers, array $cookie): array {
    $results = [];

    foreach ($server as $key => $value) {
      $results[strtoupper($key)] = $value;
    }

    $results = array_merge(
      $results,
      $this->formatHttpHeadersIntoServerVariables($headers, $cookie)
    );

    if (isset($results['REQUEST_URI'], $results['QUERY_STRING']) &&
      strlen($results['QUERY_STRING']) > 0 &&
      strpos($results['REQUEST_URI'], '?') === FALSE) {
      $results['REQUEST_URI'] .= '?'.$results['QUERY_STRING'];
    }

    return $this->correctHeadersSetIncorrectlyByPhpDevServer($results);
  }

  /**
   * Format the given HTTP headers into properly formatted $_SERVER variables.
   *
   * @param array $headers
   * @return array
   */
  protected function formatHttpHeadersIntoServerVariables(array $headers, array $cookie): array {
    $results = [];

    foreach ($headers as $key => $value) {
      $key = strtoupper(str_replace('-', '_', $key));

      if (!in_array($key, ['HTTPS', 'REMOTE_ADDR', 'SERVER_PORT'])) {
        $key = 'HTTP_'.$key;
      }

      $results[$key] = $value;
    }

    $results['HOST'] = $results['HTTP_HOST'];

    $results['HTTPS'] = 'off';
    if (isset($headers['x-forwarded-proto']) && strtoupper($headers['x-forwarded-proto']) == 'HTTPS') {
      $results['HTTPS'] = 'on';
    }

    $cookies = [];
    foreach ($cookie as $cookie_key => $cookie_value) {
      $cookies[] = $cookie_key . '=' . $cookie_value;
    }
    $results['HTTP_COOKIE'] = implode('; ', $cookies);

    // This is added for the page "Status report".
    if (extension_loaded('swoole')) {
      $results['SERVER_SOFTWARE'] = 'Swoole/' . phpversion('swoole');
    }
    elseif (extension_loaded('openswoole')) {
      $results['SERVER_SOFTWARE'] = 'Open Swoole/' . phpversion('openswoole');
    }

    return $results;
  }

  /**
   * Correct headers set incorrectly by built-in PHP development server.
   *
   * @param array $headers
   * @return array
   */
  protected function correctHeadersSetIncorrectlyByPhpDevServer(array $headers): array {
    if (array_key_exists('HTTP_CONTENT_LENGTH', $headers)) {
      $headers['CONTENT_LENGTH'] = $headers['HTTP_CONTENT_LENGTH'];
    }

    if (array_key_exists('HTTP_CONTENT_TYPE', $headers)) {
      $headers['CONTENT_TYPE'] = $headers['HTTP_CONTENT_TYPE'];
    }

    return $headers;
  }

}
