<?php

namespace Drupal\swoole\Session;

use Drupal\Core\StackMiddleware\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Overrides the core StackMiddleware class Session.
 */
class StackMiddlewareSession extends Session {

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MAIN_REQUEST, $catch = TRUE): Response {
    $asynchronous = $this->container->getParameter('asynchronous');
    if ($type === self::MAIN_REQUEST && (PHP_SAPI !== 'cli' || $asynchronous)) {
      $session = $this->container->get($this->sessionServiceName);
      $session->start();
      $request->setSession($session);
    }

    $result = $this->httpKernel->handle($request, $type, $catch);

    if ($type === self::MAIN_REQUEST && $request->hasSession()) {
      $request->getSession()->save();
    }

    return $result;
  }

}
