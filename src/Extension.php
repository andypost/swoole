<?php

namespace Drupal\swoole;

use Swoole\Process;

/**
 * The Swoole extension.
 */
class Extension {

  /**
   * Determine if the Swoole extension is installed.
   *
   * @return bool
   *   Is the Swoole or OpenSwoole extension is installed.
   */
  public function isInstalled(): bool {
    return extension_loaded('swoole') || extension_loaded('openswoole');
  }

  /**
   * Send a signal to the given process.
   *
   * @param int $process_id
   *   The process identifier.
   * @param int $signal
   *   The signal to send to the process.
   *
   * @return bool
   *   The result of the process signal.
   */
  public function dispatchProcessSignal(int $process_id, int $signal): bool {
    if ($process_id === -1) {
      return FALSE;
    }

    if (Process::kill($process_id, 0)) {
      return Process::kill($process_id, $signal);
    }

    return FALSE;
  }

  /**
   * Set the current process name.
   *
   * @param string $site_name
   *   The Drupal site name.
   * @param string $process_name
   *   The Linux process name.
   *
   * @return void
   */
  public function setProcessName(string $site_name, string $process_name): void {
    if (PHP_OS_FAMILY === 'Linux') {
      cli_set_process_title('swoole_http_server: ' . $process_name . ' for ' . $site_name);
    }
  }

  /**
   * Get the number of CPUs detected on the system.
   *
   * @return int
   *   The number of CPU's in the server.
   */
  public function cpuCount(): int {
    return swoole_cpu_num();
  }

}
