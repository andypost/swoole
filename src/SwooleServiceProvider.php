<?php

namespace Drupal\swoole;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * The service provider for the swoole module
 */
class SwooleServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Replace the class of the 'session_manager' service.
    $container->getDefinition('session_manager')
      ->setClass('Drupal\swoole\Session\SessionManager');

    // Replace the class of the 'http_middleware.session' service.
    $container->getDefinition('http_middleware.session')
      ->setClass('Drupal\swoole\Session\StackMiddlewareSession');
  }

}
