<?php

namespace Drupal\swoole\Events;

use Drupal\Core\DrupalKernel;

/**
 * The worker starting event.
 */
class WorkerStarting {

  /**
   * Constructs a WorkerStarting object.
   *
   * @param \Drupal\Core\DrupalKernel $kernel
   *   The Drupal kernel.
   */
  public function __construct(
    public DrupalKernel $kernel
  ) {}

}
