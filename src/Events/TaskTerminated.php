<?php

namespace Drupal\swoole\Events;

use Drupal\Core\DrupalKernel;

/**
 * The task terminated event.
 */
class TaskTerminated implements OperationTerminatedInterface {

  use HasKernelAndSandbox;

  /**
   * Constructs a TaskTerminated object.
   *
   * @param \Drupal\Core\DrupalKernel $kernel
   *   The Drupal kernel.
   * @param \Drupal\Core\DrupalKernel $sandbox
   *   The sandboxed Drupal kernel.
   * @param mixed $data
   *   The data for the event
   * @param mixed $result
   *   The result of the event.
   */
  public function __construct(
    public DrupalKernel $kernel,
    public DrupalKernel $sandbox,
    public $data,
    public $result
  ) {}

}
