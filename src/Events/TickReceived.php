<?php

namespace Drupal\swoole\Events;

use Drupal\Core\DrupalKernel;

/**
 * The tick recieved event.
 */
class TickReceived {

  /**
   * Constructs a TickReceived object.
   *
   * @param \Drupal\Core\DrupalKernel $kernel
   *   The Drupal kernel.
   * @param \Drupal\Core\DrupalKernel $sandbox
   *   The sandboxed Drupal kernel.
   */
  public function __construct(
    public DrupalKernel $kernel,
    public DrupalKernel $sandbox
  ) {}

}
