<?php

namespace Drupal\swoole\Events;

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * The request handeled event.
 */
class RequestHandled {

  /**
   * Constructs a RequestHandeled object.
   *
   * @param \Drupal\Core\DrupalKernel $sandbox
   *   The sandboxed Drupal kernel.
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The Drupal request.
   * @param Symfony\Component\HttpFoundation\Response $response
   *   The Drupal response.
   */
  public function __construct(
    public DrupalKernel $sandbox,
    public Request $request,
    public Response $response
  ) {}

}
