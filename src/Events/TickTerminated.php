<?php

namespace Drupal\swoole\Events;

use Drupal\Core\DrupalKernel;

/**
 * The tick terminated event.
 */
class TickTerminated implements OperationTerminatedInterface {

  use HasKernelAndSandbox;

  /**
   * Constructs a TickTerminated object.
   *
   * @param \Drupal\Core\DrupalKernel $kernel
   *   The Drupal kernel.
   * @param \Drupal\Core\DrupalKernel $sandbox
   *   The sandboxed Drupal kernel.
   */
  public function __construct(
    public DrupalKernel $kernel,
    public DrupalKernel $sandbox
  ) {}

}
