<?php

namespace Drupal\swoole\Events;

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;

/**
 * The request terminated event.
 */
class RequestReceived {

  /**
   * Constructs a RequestReceived object.
   *
   * @param \Drupal\Core\DrupalKernel $kernel
   *   The Drupal kernel.
   * @param \Drupal\Core\DrupalKernel $sandbox
   *   The sandboxed Drupal kernel.
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The Drupal request.
   */
  public function __construct(
    public DrupalKernel $kernel,
    public DrupalKernel $sandbox,
    public Request $request
  ) {}

}
