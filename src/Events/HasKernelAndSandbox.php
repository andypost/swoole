<?php

namespace Drupal\swoole\Events;

use Drupal\Core\DrupalKernel;

/**
 * The trait for returning the kernel or the sandbox.
 */
trait HasKernelAndSandbox {

  /**
   * Get the base kernel instance.
   *
   * @return \Drupal\Core\DrupalKernel
   */
  public function kernel(): DrupalKernel {
    return $this->kernel;
  }

  /**
   * Get the sandbox version of the kernel instance.
   *
   * @return \Drupal\Core\DrupalKernel
   */
  public function sandbox(): DrupalKernel {
    return $this->sandbox;
  }

}
