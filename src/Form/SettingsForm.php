<?php

namespace Drupal\swoole\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\swoole\Extension;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Swoole settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The Swoole extension.
   *
   * @var \Drupal\swoole\Extension
   */
  protected $extension;

  /**
   * Swoole SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\swoole\Extension $extension
   *   The Swoole extension.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Extension $extension) {
    parent::__construct($config_factory);
    $this->extension = $extension;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('swoole.extension')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'swoole_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['swoole.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $swoole_config = $this->config('swoole.settings');

    $form['swoole_server_main'] = [
      '#type' => 'details',
      '#title' => $this->t('Swoole server'),
      '#open' => TRUE,
    ];
    $form['swoole_server_main']['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host name'),
      '#default_value' => $swoole_config->get('host'),
      '#description' => $this->t('The host DNS name or the IP address to use for the Swoole server. For more information, go to <a href=":url">Swoole\Server::__construct</a>.', [':url' => 'https://openswoole.com/docs/modules/swoole-server-construct']),
      '#required' => TRUE,
    ];
    $form['swoole_server_main']['port'] = [
      '#type' => 'number',
      '#title' => $this->t('Port'),
      '#default_value' => $swoole_config->get('port'),
      '#description' => $this->t('The port number to use for the Swoole server. For more information, go to <a href=":url">Swoole\Server::__construct</a>.', [':url' => 'https://openswoole.com/docs/modules/swoole-server-construct']),
      '#min' => 1,
      '#size' => 10,
      '#required' => TRUE,
    ];
    $form['swoole_server_main']['site_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site path'),
      '#default_value' => $swoole_config->get('site_path'),
      '#description' => $this->t("The directory where the settings.php is located, relative to the website root directory. The default directory is 'sites/default'."),
      '#required' => TRUE,
    ];

    $form['swoole_server_ssl'] = [
      '#type' => 'details',
      '#title' => $this->t('Swoole server SSL'),
      '#open' => TRUE,
    ];
    $form['swoole_server_ssl']['ssl'] = [
      '#type' => 'radios',
      '#title' => $this->t('SSL'),
      '#default_value' => $swoole_config->get('ssl') ? 'on' : 'off',
      '#options' => ['on' => $this->t('On'), 'off' => $this->t('Off')],
      '#description' => $this->t('Use SSL (Secure Sockets Layer) for encryption based communications. For more information, go to <a href=":url">Swoole Server configuration</a>.', [':url' => 'https://openswoole.com/docs/modules/swoole-http-server-doc#swoole-http-server-configuration']),
    ];
    $form['swoole_server_ssl']['ssl_cert_file'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SSL cert file'),
      '#default_value' => $swoole_config->get('ssl_cert_file'),
      '#description' => $this->t("The location and name of the SSL cert file. For more information, go to <a href=':url'>Swoole Server ssl_cert_file</a>.", [':url' => 'https://openswoole.com/docs/modules/swoole-server/configuration#ssl_cert_file-and-ssl_key_file']),
      '#states' => [
        'visible' => [
          ':input[name="ssl"]' => ['value' => 'on'],
        ],
        'required' => [
          ':input[name="ssl"]' => ['value' => 'on'],
        ],
      ],
    ];
    $form['swoole_server_ssl']['ssl_key_file'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SSL key file'),
      '#default_value' => $swoole_config->get('ssl_key_file'),
      '#description' => $this->t("The location and name of the SSL key file. For more information, go to <a href=':url'>Swoole Server ssl_key_file</a>.", [':url' => 'https://openswoole.com/docs/modules/swoole-server/configuration#ssl_cert_file-and-ssl_key_file']),
      '#states' => [
        'visible' => [
          ':input[name="ssl"]' => ['value' => 'on'],
        ],
        'required' => [
          ':input[name="ssl"]' => ['value' => 'on'],
        ],
      ],
    ];

    $form['swoole_server_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Swoole server options'),
      '#open' => TRUE,
    ];
    $form['swoole_server_options']['daemonize'] = [
      '#type' => 'radios',
      '#title' => $this->t('Daemonize'),
      '#default_value' => $swoole_config->get('daemonize') ? 'on' : 'off',
      '#options' => ['on' => $this->t('On'), 'off' => $this->t('Off')],
      '#description' => $this->t('Daemonize the Swoole Server process. For more information, go to <a href=":url">Swoole Server daemonize</a>.', [':url' => 'https://openswoole.com/docs/modules/swoole-server/configuration#daemonize']),
    ];
    $form['swoole_server_options']['ticks'] = [
      '#type' => 'radios',
      '#title' => $this->t('Ticks'),
      '#default_value' => $swoole_config->get('ticks') ? 'on' : 'off',
      '#options' => ['on' => $this->t('On'), 'off' => $this->t('Off')],
      '#description' => $this->t('Use ticks for communication with the Swoole workers.'),
    ];
    $form['swoole_server_options']['max_execution_time'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum execution time (in seconds)'),
      '#size' => 10,
      '#min' => 1,
      '#default_value' => $swoole_config->get('max_execution_time'),
      '#description' => $this->t('The maximum execution time in seconds for a user request.'),
    ];
    $form['swoole_server_options']['package_max_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Package maximum length (in MB)'),
      '#size' => 10,
      '#min' => 1,
      '#default_value' => $swoole_config->get('package_max_length'),
      '#description' => $this->t('The maximum length of a package in MB. For more information, go to <a href=":url">Swoole Server package_max_length</a>.', [':url' => 'https://openswoole.com/docs/modules/swoole-server/configuration#package_max_length']),
    ];
    $form['swoole_server_options']['socket_buffer_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Socker buffer size (in MB)'),
      '#size' => 10,
      '#min' => 1,
      '#default_value' => $swoole_config->get('socket_buffer_size'),
      '#description' => $this->t('Set the maximum buffer size (in MB) of the socket connection for clients. For more information, go to <a href=":url">Swoole Server socket_buffer_size</a>.', [':url' => 'https://openswoole.com/docs/modules/swoole-server/configuration#socket_buffer_size']),
    ];

    $form['swoole_server_worker'] = [
      '#type' => 'details',
      '#title' => $this->t('Swoole server workers'),
      '#open' => TRUE,
    ];
    $form['swoole_server_worker']['worker_num'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of workers'),
      '#size' => 10,
      '#min' => 0,
      '#default_value' => $swoole_config->get('worker_num') ?? $this->extension->cpuCount(),
      '#description' => $this->t('The number of worker processes to start. The default value is the number of CPU cores. For more information, go to <a href=":url">Swoole Server worker_num</a>.', [':url' => 'https://openswoole.com/docs/modules/swoole-server/configuration#worker_num']),
    ];
    $form['swoole_server_worker']['max_request'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum request before worker is restarted'),
      '#size' => 10,
      '#min' => 0,
      '#default_value' => $swoole_config->get('max_request'),
      '#description' => $this->t('The maximum number of requests before the worker is restarted. When set to zero the worker does not get restarted. For more information, go to <a href=":url">Swoole Server max_request</a>.', [':url' => 'https://openswoole.com/docs/modules/swoole-server/configuration#max_request']),
    ];

    $form['swoole_server_taskworker'] = [
      '#type' => 'details',
      '#title' => $this->t('Swoole server taskworkers'),
      '#open' => TRUE,
    ];
    $form['swoole_server_taskworker']['task_worker_num'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of taskworkers'),
      '#size' => 10,
      '#min' => 0,
      '#default_value' => $swoole_config->get('task_worker_num') ?? $this->extension->cpuCount(),
      '#description' => $this->t('The number of taskworker processes to start. The default value is the number of CPU cores. For more information, go to <a href=":url">Swoole Server task_worker_num</a>.', [':url' => 'https://openswoole.com/docs/modules/swoole-server/configuration#task_worker_num']),
    ];
    $form['swoole_server_taskworker']['task_max_request'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum request before taskworker is restarted'),
      '#size' => 10,
      '#min' => 0,
      '#default_value' => $swoole_config->get('task_max_request'),
      '#description' => $this->t('The maximum number of requests before the taskworker is restarted. When set to zero the taskworker does not get restarted. For more information, go to <a href=":url">Swoole Server task_max_request</a>.', [':url' => 'https://openswoole.com/docs/modules/swoole-server/configuration#task_max_request']),
    ];

    $form['swoole_server_logging'] = [
      '#type' => 'details',
      '#title' => $this->t('Swoole server logging'),
      '#open' => TRUE,
    ];
    $form['swoole_server_logging']['log_level'] = [
      '#type' => 'radios',
      '#title' => $this->t('Log level'),
      '#default_value' => $swoole_config->get('log_level'),
      '#options' => [
        '0' => $this->t('Debug'),
        '1' => $this->t('Trace'),
        '2' => $this->t('Info'),
        '3' => $this->t('Notice'),
        '4' => $this->t('Warning'),
        '5' => $this->t('Error'),
      ],
      '#description' => $this->t('What should be logged by the Swoole server. For "Debug" the Swoole server needs to enable "--enable-debug-log" during compilation. For "Trace" the Swoole server needs to enable "--enable-trace-log" during compilation. For more information, go to <a href=":url">Swoole Server log_level</a>.', [':url' => 'https://openswoole.com/docs/modules/swoole-server/configuration#log_level']),
    ];
    $form['swoole_server_logging']['log_file'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Log file'),
      '#default_value' => $swoole_config->get('log_file'),
      '#description' => $this->t("The location and name of the log file. It defaults to 'sites/default/files/swoole.log'. For more information, go to <a href=':url'>Swoole Server log_file</a>.", [':url' => 'https://openswoole.com/docs/modules/swoole-server/configuration#log_file']),
    ];

    $form['swoole_server_console'] = [
      '#type' => 'details',
      '#title' => $this->t('Swoole server console'),
      '#open' => TRUE,
    ];
    $form['swoole_server_console']['stream_to_console'] = [
      '#type' => 'radios',
      '#title' => $this->t('Stream request data to console'),
      '#default_value' => $swoole_config->get('stream_to_console') ? 'on' : 'off',
      '#options' => ['on' => $this->t('On'), 'off' => $this->t('Off')],
      '#description' => $this->t('Stream request data to the console.'),
    ];
    $form['swoole_server_console']['add_memory_usage'] = [
      '#type' => 'radios',
      '#title' => $this->t('Memory'),
      '#default_value' => $swoole_config->get('add_memory_usage') ? 'on' : 'off',
      '#options' => ['on' => $this->t('On'), 'off' => $this->t('Off')],
      '#description' => $this->t('Add the memory usage to the request data.'),
      '#states' => [
        'visible' => [
          ':input[name="stream_to_console"]' => ['value' => 'on'],
        ],
      ],
    ];
    $form['swoole_server_console']['add_handle_duration'] = [
      '#type' => 'radios',
      '#title' => $this->t('Duration'),
      '#default_value' => $swoole_config->get('add_handle_duration') ? 'on' : 'off',
      '#options' => ['on' => $this->t('On'), 'off' => $this->t('Off')],
      '#description' => $this->t('Add the server handle duration to the request data.'),
      '#states' => [
        'visible' => [
          ':input[name="stream_to_console"]' => ['value' => 'on'],
        ],
      ],
    ];

    $form['swoole_server_process'] = [
      '#type' => 'details',
      '#title' => $this->t('Swoole server process identifiers'),
      '#open' => TRUE,
    ];
    $form['swoole_server_process']['master_pid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Master process ID'),
      '#default_value' => $swoole_config->get('master_pid'),
      '#description' => $this->t("The master process identifier, is used to communicate with the master process."),
      '#size' => 10,
      '#disabled' => TRUE,
    ];
    $form['swoole_server_process']['manager_pid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Manager process ID'),
      '#default_value' => $swoole_config->get('manager_pid'),
      '#description' => $this->t("The manager process identifier, is used to communicate with the manager process."),
      '#size' => 10,
      '#disabled' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $site_path = $form_state->getValue('site_path');
    if (substr($site_path, 0, 1) == '/') {
      $form_state->setErrorByName('site_path', $this->t("The site path should not start with a slash character."));
    }
    elseif (substr($site_path, -1, 1) == '/') {
      $form_state->setErrorByName('site_path', $this->t("The site path should not end with a slash character."));
    }
    elseif (!file_exists(DRUPAL_ROOT . '/' . $site_path . '/settings.php')) {
      $form_state->setErrorByName('site_path', $this->t("The file settings.php does not exist in the directory: '%path'", ['%path' => $form_state->getValue('site_path')]));
    }

    $ssl = $form_state->getValue('ssl');
    if ($ssl == 'on') {
      if (!file_exists(DRUPAL_ROOT . '/' . $form_state->getValue('ssl_cert_file'))) {
        $form_state->setErrorByName('ssl_cert_file', $this->t("The SSL cert file does not exist."));
      }
      if (!file_exists(DRUPAL_ROOT . '/' . $form_state->getValue('ssl_key_file'))) {
        $form_state->setErrorByName('ssl_key_file', $this->t("The SSL key file does not exist."));
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('swoole.settings')
      ->set('host', $form_state->getValue('host'))
      ->set('port', (int) $form_state->getValue('port'))
      ->set('site_path', $form_state->getValue('site_path'))
      ->set('daemonize', ($form_state->getValue('daemonize') == 'on' ? TRUE : FALSE))
      ->set('ssl', ($form_state->getValue('ssl') == 'on' ? TRUE : FALSE))
      ->set('ssl_cert_file', $form_state->getValue('ssl_cert_file'))
      ->set('ssl_key_file', $form_state->getValue('ssl_key_file'))
      ->set('max_execution_time', (int) $form_state->getValue('max_execution_time'))
      ->set('ticks', ($form_state->getValue('ticks') == 'on' ? TRUE : FALSE))
      ->set('max_request', (int) $form_state->getValue('max_request'))
      ->set('worker_num', (int) $form_state->getValue('worker_num'))
      ->set('task_worker_num', (int) $form_state->getValue('task_worker_num'))
      ->set('package_max_length', (int) $form_state->getValue('package_max_length'))
      ->set('socket_buffer_size', (int) $form_state->getValue('socket_buffer_size'))
      ->set('log_level', (int) $form_state->getValue('log_level'))
      ->set('log_file', $form_state->getValue('log_file'))
      ->set('stream_to_console', ($form_state->getValue('stream_to_console') == 'on' ? TRUE : FALSE))
      ->set('add_memory_usage', ($form_state->getValue('add_memory_usage') == 'on' ? TRUE : FALSE))
      ->set('add_handle_duration', ($form_state->getValue('add_handle_duration') == 'on' ? TRUE : FALSE))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
