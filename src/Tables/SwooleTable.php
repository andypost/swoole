<?php

namespace Drupal\swoole\Tables;

use Swoole\Table;

if (SWOOLE_VERSION_ID === 40804 || SWOOLE_VERSION_ID >= 50000) {

  /**
   * The Swoole memory table.
   */
  class SwooleTable extends Table {

    use EnsuresColumnSizesTrait;

    /**
     * The table columns.
     *
     * @var array
     */
    protected $columns;

    /**
     * Set the data type and size of the columns.
     *
     * @param string $name
     * @param int $type
     * @param int $size
     * @return bool
     */
    public function column(string $name, int $type, int $size = 0): bool {
      $this->columns[$name] = [$type, $size];

      return parent::column($name, $type, $size);
    }

    /**
     * Update a row of the table.
     *
     * @param string $key
     * @param array $values
     * @return bool
     */
    public function set(string $key, array $values): bool {
      $this->ensureColumnsSize();

      return parent::set($key, $values);
    }
  }
}
else {

  /**
   * The Swoole memory table.
   */
  class SwooleTable extends Table {

    use EnsuresColumnSizesTrait;

    /**
     * The table columns.
     *
     * @var array
     */
    protected $columns;

    /**
     * Set the data type and size of the columns.
     *
     * @param string $name
     * @param int $type
     * @param int $size
     * @return void
     */
    public function column($name, $type, $size = 0) {
      $this->columns[$name] = [$type, $size];

      parent::column($name, $type, $size);
    }

    /**
     * Update a row of the table.
     *
     * @param string $key
     * @param array $values
     * @return void
     */
    public function set($key, array $values) {
      $this->ensureColumnsSize();

      parent::set($key, $values);
    }
  }

}
