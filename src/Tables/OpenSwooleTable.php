<?php

namespace Drupal\swoole\Tables;

use Swoole\Table;

/**
 * The OpenSwoole memory table.
 */
class OpenSwooleTable extends Table {

  use EnsuresColumnSizesTrait;

  /**
   * The table columns.
   *
   * @var array
   */
  protected $columns;

  /**
   * Set the data type and size of the columns.
   *
   * @param string $name
   * @param int $type
   * @param int $size
   * @return bool
   */
  public function column($name, $type, $size = 0): bool {
    $this->columns[$name] = [$type, $size];

    return parent::column($name, $type, $size);
  }

  /**
   * Update a row of the table.
   *
   * @param string $key
   * @param array $values
   * @return bool
   */
  public function set($key, array $values): bool {
    $this->ensureColumnsSize();

    return parent::set($key, $values);
  }

}
