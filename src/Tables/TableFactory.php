<?php

namespace Drupal\swoole\Tables;

/**
 * The factory for creating Swoole memory tables.
 */
class TableFactory {

  /**
   * Creates a new Swoole Table with the given size.
   *
   * @return \Swoole\Table
   */
  public static function make($size) {
    return extension_loaded('openswoole') ? new OpenSwooleTable($size) : new SwooleTable($size);
  }

}
