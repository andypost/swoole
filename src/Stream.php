<?php

namespace Drupal\swoole;

use Drupal\Core\Utility\Error;

/**
 * Stream swoole data to the console.
 */
class Stream {

  /**
   * Stream the given request information to stdout.
   *
   * @param string $method
   *   The used method.
   * @param string $url
   *   The used URL.
   * @param float $duration
   *   The duration.
   *
   * @return void
   */
  public static function request(string $method, string $url, int $memory, float $duration) {
    fwrite(STDOUT, json_encode([
      'stream_type' => 'request',
      'method' => $method,
      'url' => $url,
      'memory' => $memory,
      'duration' => $duration,
    ])."\n");
  }

  /**
   * Stream the given throwable to stderr.
   *
   * @param \Throwable $throwable
   *   The thrown exception.
   *
   * @return void
   */
  public static function throwable(\Throwable $throwable) {
    $error = Error::decodeException($throwable);
    $error['stream_type'] = 'throwable';
    fwrite(STDERR, json_encode($error)."\n");
  }

}
