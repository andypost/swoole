<?php

namespace Drupal\swoole;

use Drupal\Core\DrupalKernel;
use Drupal\Core\Http\TrustedHostsRequestFactory;
use Drupal\Core\Site\Settings;
use Drupal\swoole\Events\TaskReceived;
use Drupal\swoole\Events\TaskTerminated;
use Drupal\swoole\Events\TickReceived;
use Drupal\swoole\Events\TickTerminated;
use Drupal\swoole\Events\RequestHandled;
use Drupal\swoole\Events\RequestReceived;
use Drupal\swoole\Events\RequestTerminated;
use Drupal\swoole\Events\WorkerErrorOccurred;
use Drupal\swoole\Events\WorkerStarting;
use Drupal\swoole\Events\WorkerStopping;
use Drupal\swoole\Exceptions\TaskExceptionResult;
use Drupal\swoole\ServesStaticFilesInterface;
use Drupal\swoole\Stream;
use Drupal\swoole\TaskResult;
use Swoole\Http\Response as SwooleResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * The worker that handles the user request.
 */
class Worker implements WorkerInterface {

  use DispatchesEvents;

  /**
   * Stream the request data to the console.
   *
   * @var bool
   */
  protected $streamToConsole;

  /**
   * Add memory usage to the console.
   *
   * @var bool
   */
  protected $addMemoryUsage;

  /**
   * Add handle duration to the console.
   *
   * @var bool
   */
  protected $addHandleDuration;

  /**
   * Constructs a Worker object.
   *
   * @param \Drupal\Core\DrupalKernel $kernel
   *   The Drupal kernel factory instance.
   * @param \Drupal\swoole\ClientInterface $client
   *   The client instance.
   */
  public function __construct(
    protected DrupalKernel $kernel,
    protected ClientInterface $client
  ) {}

  /**
   * Boot / initialize the Swoole worker.
   *
   * @return void
   */
  public function boot(): void {
    $this->dispatchEvent($this->kernel, new WorkerStarting($this->kernel));
  }

  /**
   * Handle an incoming request and send the response to the client.
   *
   * @param \Symfony\Component\HttpFoundation\Request $drupal_request
   *   The Drupal request.
   * @param \Swoole\Http\Response $swoole_response
   *   The Swoole response.
   * @param string $drupal_root
   *   Absolute path to the root of the Drupal installation.
   *
   * @return void
   */
  public function handle(Request $drupal_request, SwooleResponse $swoole_response, string $drupal_root): void {
    $start_time = microtime(TRUE);

    // Initialize our list of trusted HTTP Host headers to protect against
    // header attacks.
    $host_patterns = Settings::get('trusted_host_patterns', []);
    if (!empty($host_patterns)) {
      $drupal_request::setTrustedHosts($host_patterns);

      // Get the host, which will validate the current request.
      try {
        $host = $drupal_request->getHost();

        // Fake requests created through Request::create() without passing in the
        // server variables from the main request have a default host of
        // 'localhost'. If 'localhost' does not match any of the trusted host
        // patterns these fake requests would fail the host verification. Instead,
        // TrustedHostsRequestFactory makes sure to pass in the server variables
        // from the main request.
        $request_factory = new TrustedHostsRequestFactory($host);
        Request::setFactory([$request_factory, 'createRequest']);
      }
      catch (\UnexpectedValueException $e) {
        throw new \Symfony\Component\HttpKernel\Exception\BadRequestHttpException('The provided host name is not valid for this server.');
      }
    }

    // For static files.
    if ($this->client instanceof ServesStaticFilesInterface && $this->client->canServeRequestAsStaticFile($drupal_request, $drupal_root)) {
      $this->client->serveStaticFile($drupal_request, $swoole_response, $drupal_root);
      return;
    }

    // We will clone the kernel instance so that we have a clean copy to switch
    // back to once the request has been handled. This allows us to easily delete
    // certain instances that got resolved / mutated during a previous request.
    $sandbox = clone $this->kernel;

    try {
      $responded = FALSE;

      ob_start();

      $this->dispatchEvent($sandbox, new RequestReceived($this->kernel, $sandbox, $drupal_request));

      try {
        $drupal_response = $sandbox->getContainer()->get('http_kernel')->handle($drupal_request);
      }
      catch (\Exception $e) {
        $drupal_response = $sandbox->handleException($e, $drupal_request, HttpKernelInterface::MAIN_REQUEST);
      }

      $this->dispatchEvent($sandbox, new RequestHandled($sandbox, $drupal_request, $drupal_response));

      // Adapt response headers to the current request.
      $drupal_response->prepare($drupal_request);

      $sandbox->getContainer()->get('session_manager')->addCookiesToResponse($drupal_response);

      $output = ob_get_contents();

      ob_end_clean();

      // Here we will actually hand the incoming request to the Drupal kernel so
      // it can generate a response. We'll send this response back to the client so
      // it can be returned to a browser. This gateway will also dispatch events.
      $this->client->respond($swoole_response, $drupal_response, $output);

      $responded = TRUE;

      $sandbox->getContainer()->get('session_manager')->resetSessionAfterResponse();

      $sandbox->terminate($drupal_request, $drupal_response);

      $this->dispatchEvent($sandbox, new RequestTerminated($this->kernel, $sandbox, $drupal_request, $drupal_response));

      if ($this->streamToConsole) {
        Stream::request(
          $drupal_request->getMethod(),
          $this->getFullUrl($drupal_request),
          $this->addMemoryUsage ? memory_get_usage() : 0,
          $this->addHandleDuration ? microtime(TRUE) - $start_time : 0
        );
      }
    }
    catch (\Throwable $e) {
      $this->handleWorkerError($e, $sandbox, $drupal_request, $swoole_response, $responded);
    }

    // After the request handling process has completed we will unset some variables
    // plus reset the current kernel state back to its original state before
    // it was cloned. Then we will be ready for the next worker iteration loop.
    unset($sandbox, $drupal_request, $drupal_response, $output);
  }

  /**
   * Get the full URL.
   *
   * @param \Symfony\Component\HttpFoundation\Request $drupal_request
   *   The Drupal request.
   *
   * @return string
   */
  protected function getFullUrl(Request $drupal_request) {
    if ($query_string = $drupal_request->getQueryString()) {
      $question = $drupal_request->getBaseUrl() . $drupal_request->getPathInfo() === '/' ? '/?' : '?';

      return $drupal_request->url() . $question . $query_string;
    }
    else {
      return rtrim(preg_replace('/\?.*/', '', $drupal_request->getUri()), '/');
    }
  }


  /**
   * Handle an incoming task.
   *
   * @param mixed $data
   *   The data to be used in the task.
   *
   * @return mixed
   */
  public function handleTask($data) {
    $result = FALSE;

    // We will clone the kernel instance so that we have a clean copy to switch
    // back to once the request has been handled. This allows us to easily delete
    // certain instances that got resolved / mutated during a previous request.
    $sandbox = clone $this->kernel;

    try {
      $this->dispatchEvent($sandbox, new TaskReceived($this->kernel, $sandbox, $data));

      $result = $data();

      $this->dispatchEvent($sandbox, new TaskTerminated($this->kernel, $sandbox, $data, $result));
    }
    catch (\Throwable $e) {
      $this->dispatchEvent($sandbox, new WorkerErrorOccurred($e, $sandbox));

      return TaskExceptionResult::from($e);
    }

    // After the request handling process has completed we will unset some variables
    // plus reset the current kernel state back to its original state before
    // it was cloned. Then we will be ready for the next worker iteration loop.
    unset($sandbox);

    return new TaskResult($result);
  }

  /**
   * Handle an incoming tick.
   *
   * @return void
   */
  public function handleTick(): void {
    $sandbox = clone $this->kernel;

    try {
      $this->dispatchEvent($sandbox, new TickReceived($this->kernel, $sandbox));
      $this->dispatchEvent($sandbox, new TickTerminated($this->kernel, $sandbox));
    }
    catch (\Throwable $e) {
      $this->dispatchEvent($sandbox, new WorkerErrorOccurred($e, $sandbox));
    }

    unset($sandbox);
  }

  /**
   * Handle an uncaught exception from the worker.
   *
   * @param \Throwable $e
   *   The thrown exception.
   * @param \Drupal\Core\DrupalKernel $kernel
   *   The Drupal kernel.
   * @param \Symfony\Component\HttpFoundation\Request $drupal_request
   *   The Drupal request.
   * @param \Swoole\Http\Response $swoole_response
   *   The Swoole response.
   * @param bool $hasResponded
   *   Has the response been send.
   *
   * @return void
   */
  protected function handleWorkerError(\Throwable $e, DrupalKernel $kernel, Request $drupal_request, SwooleResponse $swoole_response, bool $hasResponded  ): void {
    if (!$hasResponded) {
      $this->client->error($e, $kernel, $drupal_request, $swoole_response);
    }

    $this->dispatchEvent($kernel, new WorkerErrorOccurred($e, $kernel));
  }

  /**
   * Get the kernel instance being used by the worker.
   *
   * @return \Drupal\Core\DrupalKernel
   */
  public function kernel(): DrupalKernel {
    if (!$this->kernel) {
      throw new \RuntimeException('Worker has not booted. Unable to access the Drupal kernel.');
    }

    return $this->kernel;
  }

  /**
   * Terminate the worker.
   *
   * @return void
   */
  public function terminate(): void {
    $this->dispatchEvent($this->kernel, new WorkerStopping($this->kernel));
  }

  /**
   * Stream request data to the console settings.
   *
   * @param bool $stream_to_console
   *   Stream the request data to the console.
   * @param bool $add_memory_usage
   *   Add memory usage to the console.
   * @param bool $add_handle_duration
   *   Add handle duration to the console.
   *
   * @return void
   */
  public function streamRequestDataToConsoleSettings($stream_to_console = FALSE, $add_memory_usage = FALSE, $add_handle_duration = FALSE): void {
    $this->streamToConsole = $stream_to_console;
    $this->addMemoryUsage = $add_memory_usage;
    $this->addHandleDuration = $add_handle_duration;
  }

}
