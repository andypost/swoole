<?php

namespace Drupal\swoole;

use Symfony\Component\HttpFoundation\Request;
use Swoole\Http\Response as SwooleResponse;

/**
 * Defines the interface for static files to the client.
 */
interface ServesStaticFilesInterface {

  /**
   * Determine if the request can be served as a static file.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $drupal_root
   *   Absolute path to the root of the Drupal installation.
   *
   * @return bool
   */
  public function canServeRequestAsStaticFile(Request $request, string $drupal_root): bool;

  /**
   * Serve the static file that was requested.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Swoole\Http\Response $swoole_response
   *   The Swoole response.
   *
   * @param string $drupal_root
   *   Absolute path to the root of the Drupal installation.
   *
   * @return void
   */
  public function serveStaticFile(Request $request, SwooleResponse $swoole_response, string $drupal_root): void;

}
